package com.company;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.jar.JarEntry;

public class Main {
    public static void main(String[] args)
    {
        JFrame f = new JFrame("Border Layout");

        // -------------------------------------------------------
        // Make Panel with 5 button
        // -------------------------------------------------------
        JPanel MyPanel1 = new JPanel();

        MyPanel1.setLayout(new GridLayout(3, 2));

        JLabel petNameLabel = new JLabel("Pet Name");
        String[] choices = { "波利", "獸人戰士", "獸人戰士長"};
        JTextField waitingNameTextField = new JTextField(100);

        MyPanel1.add(petNameLabel);
//        MyPanel1.add(petNameField);
        final JComboBox<String> cb = new JComboBox<String>(choices);
        MyPanel1.add(cb);
        MyPanel1.add(new JLabel("Waiting time"));
        MyPanel1.add(waitingNameTextField);
       // petList.addActionListener(this);
       // MyPanel1.add(petNameLabel,BorderLayout.WEST);
        f.getContentPane().add( MyPanel1, "North");   // Add MyPanel1 to North

        // -----------------------------------------------------------
        // Make another Panel with 5 button
        // -----------------------------------------------------------
        JPanel MyPanel2 = new JPanel();

        MyPanel2.setLayout( new BorderLayout() );

        JButton x6 = new JButton("I am x6");
        JButton x7 = new JButton("I am x7");
        JButton x8 = new JButton("I am x8");
        JButton x9 = new JButton("I am x9");
        JButton x10 = new JButton("I am x10");

        MyPanel2.add(x6, "North");
        MyPanel2.add(x7, "South");
        MyPanel2.add(x8, "East");
        MyPanel2.add(x9, "West");
        MyPanel2.add(x10, "Center");

        f.getContentPane().add( MyPanel2, "South");   // Paste MyPanel2 to South
        //    f.getContentPane().add(MyPanel2, "Center");  // Paste MyPanel2 to Center

        f.setSize(500, 300);
        f.setVisible(true);
    }
}